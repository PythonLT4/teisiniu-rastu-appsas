from django.test import Client


def test_index(client: Client):
    assert client.get('/').status_code == 200
